import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should render title', () => {
    const fixture = TestBed.createComponent(HomeComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.title')?.textContent).toContain('Mon pipeline');
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have default values for number1, number2, and result', () => {
    expect(component.number1).toEqual(0);
    expect(component.number2).toEqual(0);
    expect(component.result).toEqual(0);
  });

  it('should correctly add two numbers', () => {
    component.number1 = 2;
    component.number2 = 3;
    component.add();
    expect(component.result).toEqual(5);
  });

  it('should update result when number1 or number2 changes', () => {
    component.number1 = 2;
    component.number2 = 3;
    component.add();
    expect(component.result).toEqual(5);

    component.number1 = 5;
    component.add();
    expect(component.result).toEqual(8);
  });
});
