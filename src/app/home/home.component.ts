import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  number1: number = 0;
  number2: number = 0;
  result: number = 0;

  constructor() { }

  ngOnInit(): void {
  }
  
  add(): void {
    this.result = this.number1 + this.number2;
  }

}
