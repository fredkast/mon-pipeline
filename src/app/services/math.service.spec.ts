// math.service.spec.ts

import { TestBed } from '@angular/core/testing';
import { MathService } from './math.service';

describe('MathService', () => {
  let service: MathService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MathService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add two numbers', () => {
    const result = service.add(2, 3);
    expect(result).toEqual(5);
  });

  it('should add two negative numbers', () => {
    const result = service.add(-2, -3);
    expect(result).toEqual(-5);
  });

  it('should add a positive and a negative number', () => {
    const result = service.add(2, -3);
    expect(result).toEqual(-1);
  });
});
